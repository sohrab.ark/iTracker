//
//  openCVW.m
//  iTracker
//
//  Created by sohrab on 22/11/2016.
//  Copyright © 2016 Alireza K. All rights reserved.
//

#import "openCVW.h"
#import <opencv2/opencv.hpp>
#import <opencv2/imgcodecs/ios.h>
#import "algo.h"

using namespace cv;

@implementation openCVW

+(UIImage *) imageToGray:(UIImage *)image
{
    // transform UIImage to cv::Mat
    Mat imageMat;
    UIImageToMat(image, imageMat);
    
    // if image already is grayscale return it!
    if(imageMat.channels() == 1) return image;
    
    //transfer the cv::mat color to grayscale
    Mat grayMat;
    cvtColor(imageMat, grayMat, CV_RGB2GRAY);
    
    //transfer grayMat to UIImage
    return MatToUIImage(grayMat);
    
}

+(CGPoint) getElipse:(UIImage *)image
{
    // transform UIImage to cv::Mat
    Mat imageMat;
    UIImageToMat(image, imageMat);
    
    // get elipse center
    cv::RotatedRect rect;
    rect = ELSE::run(imageMat,10);
    CGPoint p = CGPointMake(rect.center.x, rect.center.y);
    return p;
}

+(UIImage *) findCircle:(UIImage *)image withMinDist:(int)minDist withParm1:(int)p1 withParm2:(int)p2 withMinR:(int)minR withMaxR:(int)maxR
{
    Mat imageMat, imageBlur, detected_edges;
    
    UIImageToMat(image, imageMat);
    
    blur( imageMat, imageBlur, cv::Size(5, 5));
    
    std::vector<Vec3f> circles;
    
    // best values: minDistance=50, Parm1=150, Parm2=20, minR=15, maxR=30
    HoughCircles( imageBlur, circles, CV_HOUGH_GRADIENT, 1, minDist, p1, p2, minR, maxR );
    
    for( size_t i = 0; i < circles.size(); i++ )
    {
        cv::Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
        int radius = cvRound(circles[i][2]);
        // circle center
        cv::circle( imageMat, center, 3, Scalar(0,255,0), -1, 8, 0 );
        // circle outline
        cv::circle( imageMat, center, radius, Scalar(0,0,255), 3, 8, 0 );
    }
    return MatToUIImage(imageMat);
}

+(UIImage *) getEdge:(UIImage *)image withTreshold:(int)t withRatio:(int)r withKernel:(int) k withBlur:(int)b
{
    Mat detected_edges, src_gray;
    Mat threshold_output;
    std::vector<std::vector<cv::Point> > contours;
    std::vector<Vec4i> hierarchy;
    int points = 50;
    UIImageToMat(image, src_gray);
    
    /// Reduce noise with a kernel 3x3
    blur( src_gray, detected_edges, cv::Size(b,b) );
    
    /// Canny detector
    Canny( detected_edges, detected_edges,t, t*r, k );
    detected_edges.convertTo(threshold_output, CV_8U);
    
    /// Find contours
    findContours( threshold_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0) );
    
    /// Find the rotated rectangles and ellipses for each contour
    RotatedRect circle;
    RotatedRect box;
    for( int i = 0; i < contours.size(); i++ )
    {
        if( contours[i].size() > points )
        {
            box = fitEllipse( Mat(contours[i]) );
            int max = MAX(box.size.width,box.size.height);
            if((abs(box.size.width - box.size.height) / max) < 10)
            {
                circle=box;
                break;
            }
        }
    }

    
    ellipse(src_gray, box, Scalar(0,0,255), 1, LINE_AA);
    ellipse(src_gray, box.center, box.size*0.5f, box.angle, 0, 360, Scalar(0,255,255), 1, LINE_AA);
    
    
    
    /// Using Canny's output as a mask, we display our result
    /*dst = Scalar::all(0);
    
    src_gray.copyTo( dst, detected_edges);

    return MatToUIImage(dst);*/
    return MatToUIImage(src_gray);
}

+(UIImage *) getCorner:(UIImage *)image fast:(int)f
{
    Mat imageMat, imageBlur, imageEdge, dst;
    std::vector<cv::KeyPoint> corners;
    
    UIImageToMat(image, imageMat);
    
    /// Reduce noise with a kernel 3x3
    blur( imageMat, imageBlur, cv::Size(3,3) );
    
    /// Canny detector
    //Canny( imageBlur, imageEdge, 20, 150, 3 );
    
    /// Create a matrix of the same type and size as src (for dst)
    //dst.create( imageMat.size(), imageMat.type() );
    //dst = Scalar::all(0);
    
    //imageMat.copyTo(dst,imageEdge);
    // best value for f= 40
    FAST(imageBlur, corners, 10);
    
    CGPoint corner = CGPointMake(0, 0);
    int counter = 0;
    // set a point as default!
    if(corners.size() > 0)
    {
        //corner.x = corners[0].pt.x;
        //corner.y = corners[0].pt.y;
        for(int i=1;i<corners.size();i++)
        {
            if(corners[i].pt.x < 50 && corners[i].pt.x > 25 && corners[i].pt.y > 30 && corners[i].pt.y < 50)
            {
                counter++;
                corner.x += corners[i].pt.x;
                corner.y += corners[i].pt.y;
            }
        }
    }
    corner.x /= counter;
    corner.y /= counter;
    /*for(int i=0;i<corners.size();i++)
    {
        cv::Point center(cvRound(corners[i].pt.x),cvRound(corners[i].pt.y));
        // circle center
        cv::circle( imageMat, center, 3, Scalar(255,255,255), -1, 8, 0 );
        // circle outline
        //cv::circle( dst, center, 10, Scalar(255,255,255), 3, 8, 0 );
    }*/
    
    cv::Point center(cvRound(corner.x),cvRound(corner.y));
    // circle center
    cv::circle( imageBlur, center, 3, Scalar(255,255,255), -1, 8, 0 );
    
    // show the corners in image
    return MatToUIImage(imageBlur);
}

+(void)getAll:( UIImage *)image result:(inout CGFloat [5])results
{
    //withMinDist:(int)minDist withParm1:(int)p1 withParm2:(int)p2 withMinR:(int)minR withMaxR:(int)maxR
    int houghMinDistance = 50;
    int houghParm1 = 150;
    int houghParm2 = 20;
    int houghMinR = 15; // maybe for blue eyes it should be smaller!
    int houghMaxR = 30; // maybe for blue eyes it should be smaller!
    
    Mat imageMat, imageBlur, detected_edges;
    
    
    UIImageToMat(image, imageMat);
    
    blur( imageMat, imageBlur, cv::Size(5, 5));
    
    std::vector<Vec3f> circles;
    
    // best values: minDistance=50, Parm1=150, Parm2=20, minR=15, maxR=30
    HoughCircles( imageBlur, circles, CV_HOUGH_GRADIENT, 1, houghMinDistance, houghParm1, houghParm2, houghMinR, houghMaxR );
    
    // first point is center of circle
    CGPoint center = CGPointMake(0, 0);
    CGFloat r = 0;
    if (circles.size() > 0)
    {
        center.x = circles[0][0];
        center.y = circles[0][1];
        r = circles[0][2];
    }
    
    // FAST Corner detection
    std::vector<cv::KeyPoint> corners;
    int FastThreshold = 10;
    blur(imageMat, imageBlur, cv::Size(3,3));
    FAST(imageBlur, corners, FastThreshold);
    int counter = 0;
    CGPoint corner = CGPointMake(0, 0);
    
    // set a point as default!
    if(corners.size() > 0)
    {
        for(int i=1;i<corners.size();i++)
        {
            if(corners[i].pt.x < 50 && corners[i].pt.x > 25 && corners[i].pt.y > 30 && corners[i].pt.y < 50)
            {
                counter++;
                corner.x += corners[i].pt.x;
                corner.y += corners[i].pt.y;
            }
        }
    }
    corner.x /= counter;
    corner.y /= counter;
    
    cv::Point c(cvRound(corner.x),cvRound(corner.y));
    // circle center
    cv::circle( imageMat, c, 3, Scalar(255,255,255), -1, 8, 0 );
    results[0] = center.x;
    results[1] = center.y;
    results[2] = r;
    results[3] = corner.x;
    results[4] = corner.y;
}

+(void) getSVD:(const CGFloat [])screen withX: (const CGFloat [])x withY: (const CGFloat [])y withXY: (const CGFloat [])xy withXX: (const CGFloat [])xx withYY: (const CGFloat [])yy withCo:(inout CGFloat [6])co
{
    Mat input = input.ones(9, 6, CV_64F);
    Mat output;
    Mat matY = matY.ones(9,1,CV_64F);
    for(int i=0;i<9;i++)
    {
        //input.at<float32_t>(0,i) = screen[i];
        input.at<float32_t>(1,i) = x[i];
        input.at<float32_t>(2,i) = y[i];
        input.at<float32_t>(3,i) = xy[i];
        input.at<float32_t>(4,i) = xx[i];
        input.at<float32_t>(5,i) = yy[i];
        matY.at<float32_t>(0,i) = screen[i];
    }
    //SVD::compute(input, output);
    input.t();
    
    cv::solve(input,matY, output, DECOMP_SVD);
    for(int i=0;i<6;i++)
    {
        co[i] = output.at<float32_t>(0,i);
    }
    return ;
}
@end
